/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacion3;

/**
 *
 * @author Luis Adrian
 */
public class Logica {
    private int num = (int) (Math.random()* (50 + 1));
    private Personas [][] agenda;
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLUE = "\u001B[34m";
    
  public Logica(){
      agenda = new Personas[2][2];
  }
  
  public int numeroSuerte(int dia, int mes, int ano){
      int suerte = dia + mes + ano;
      
      int pN = suerte % 10;
      suerte = suerte /10;

      int sN = suerte % 10;
      suerte = suerte /10;
      
      int tN = suerte % 10;
      suerte = suerte /10;
      
      int cN = suerte % 10;
      
      int total = pN + sN +tN +cN;
     
      return total;
  }
  
  public String metodo2(int num1){
        
         int i, suma = 0;

        for (i = 1; i < num1; i++) {  // i son los divisores. Se divide desde 1 hasta n-1 
            if (num1 % i == 0) {
                suma = suma + i;     // si es divisor se suma
            }
        }
        if (suma == num1) {  // si el numero es igual a la suma de sus divisores es perfecto
            return "Perfecto";
        } 
        else if (suma < num1) { // si el numero es menor a la suma de sus divisores es deficiente
            return "deficiente";

        }
        else if (suma > num1) { // si el numero es mayor a la suma de sus divisores es abundante
            return "abundante";
    }
         return "No es nada";

        
    }
  
  public int juegoAzar(){
      
      return num;
      
  }
  
  public void generarAgenda(Personas personas){
       App:
       for (int f = 0; f < agenda.length; f++) {
            for (int c = 0; c < agenda[f].length; c++) {
                if(agenda[f][c] == null){
                  agenda[f][c] = personas;
                  break App;
                  
                }
            }
            
        }
  }
  
   public String verAgenda() {
        String str = "";
        for (int f = 0; f < agenda.length; f++) {
            for (int c = 0; c < agenda[f].length; c++) {
                if (agenda[f][c] != null) {
                    
                
                str += agenda[f][c].getNombre()+ ": ";
                if (agenda[f][c].getEdad() % 2 == 0){
                    str += ANSI_BLUE;
                }else{
                    str += ANSI_RED;
                }
                str += agenda[f][c].getEdad()+ "  ";
                str += ANSI_RESET;
            }
            str += "\n";
            }
        }
        return str;
    }
  
   public void modificarEdad(String nombre,Personas personas){
        App:
        for (int f = 0; f < agenda.length; f++) {
            for (int c = 0; c < agenda[f].length; c++) {
                if (agenda[f][c].getNombre().equals(nombre)){
                  agenda[f][c] = personas; 
                  break App;
                }else{
                    System.out.println("El Nombre no existe en la lista");
                }
                
            }
        }
       
  }
}

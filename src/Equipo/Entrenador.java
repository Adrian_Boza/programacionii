/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Equipo;

/**
 *
 * @author Luis Adrian
 */
public class Entrenador extends EmpleadosPadre{
    
    private String idFederacion;

    public Entrenador(String idFederacion, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        
        this.idFederacion = idFederacion;
    }
     public String getAtributos(){
        return "Nombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nCedula:" + id
                + "\nEdad: " + edad
                + "\nID Federación: " + idFederacion;
               
    }
     
    //polimorfismo
    public void concentrarse(){
        
        System.out.println("No estan en concentración");
        
    }
    
    //polimorfismo
    public void viajar(){
        
        System.out.println("Si viajaran");
        
    }
    
    public void dirigirPartido(){
        
        System.out.println("Si lo dirigira");
        
    }
    
    public void dirigirEntrenamiento(){
        
        System.out.println("No entrenaran");
        
    }
}

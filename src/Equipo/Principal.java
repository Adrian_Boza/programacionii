/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Equipo;

/**
 *
 * @author Luis Adrian
 */
public class Principal {
    
    public static void main(String[] args) {
        
        EmpleadosPadre ep = new EmpleadosPadre(207960222, "Luis", "Boza", 25);
        Futbolistas fut = new Futbolistas(206660444, "Adrian", "munir", 21, 9, "Delantero");
        Entrenador dt = new Entrenador("2514", 201140224, "Arturo", "Guardiola", 56);
        Masajitas msj = new Masajitas("Doctorado", 14, 402250144, "Kevin", "ferdinand", 42);
        
        System.out.println(ep.getAtributos()+"\n");
        System.out.println(fut.getAtributos()+"\n");
        tipo(fut);
        System.out.println(dt.getAtributos()+"\n");
        tipo(dt);
        System.out.println(msj.getAtributos()+"\n");
        tipo(msj);
        
    
        
    }
    
    //polimorfismo
    public static void tipo(EmpleadosPadre empleados){
        empleados.concentrarse();
        empleados.viajar();
    }
    
}

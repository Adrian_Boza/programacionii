/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Equipo;

/**
 *
 * @author Luis Adrian
 */
public class Masajitas extends EmpleadosPadre{
    
    private String titulacion;
    private int anosExperiencia;

    public Masajitas(String titulacion, int anosExperiencia, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        
        this.titulacion = titulacion;
        this.anosExperiencia = anosExperiencia;
    }
     public String getAtributos(){
        return "Nombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nCedula:" + id
                + "\nEdad: " + edad
                + "\nTitulación: " + titulacion
                + "\nAños de experencia: " + anosExperiencia;
               
    }
     //polimorfismo
    public void concentrarse(){
        
        System.out.println("No estan en concentración");
        
    }
    
    //polimorfismo
    public void viajar(){
        
        System.out.println("Si viajaran");
        
    }
    
    public void darMasajes(){
        
        System.out.println("Masajes antes y despues del partido");
        
    }
}

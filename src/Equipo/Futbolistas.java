/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Equipo;

/**
 *
 * @author Luis Adrian
 */
public class Futbolistas extends EmpleadosPadre{
    
    private int dorsal;
    private String demarcacion;
    
    public Futbolistas(int id, String nombre, String apellidos, int edad, int dorsal, String demarcacion) {
        super(id, nombre, apellidos, edad);
       
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }
    
    public String getAtributos(){
        return "Nombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nCedula:" + id
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nDemarcación: " + demarcacion;
    }
    
    //polimorfismo
    public void concentrarse(){
        
        System.out.println("Si estan en concentración");
        
    }
    
    //polimorfismo
    public void viajar(){
        
        System.out.println("Si viajaran");
        
    }
    
    public void jugarPartido(){
        
        System.out.println("Si jugaran");
        
    }
    
    public void entrenar(){
        
        System.out.println("No entrenaran");
        
    }
}

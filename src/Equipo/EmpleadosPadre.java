/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Equipo;

/**
 *
 * @author Luis Adrian
 */
public class EmpleadosPadre {
    
    public int id, edad;
    public String nombre, apellidos;
   

    public EmpleadosPadre(int id, String nombre, String apellidos, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }
    
    public String getAtributos(){
        return "Nombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nCedula:" + id
                + "\nEdad: " + edad;
    }
   
   //polimorfismo
    public void concentrarse(){
        
        System.out.println("Estan en concentración?");
        
    }
    
    //polimorfismo
    public void viajar(){
        
        System.out.println("Viajaran?");
        
    }
}
